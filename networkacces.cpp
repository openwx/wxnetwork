#include "networkacces.h"

wxDEFINE_EVENT(REQ_RETURN_DATA, RequestEvent);
wxDEFINE_EVENT(REQ_RETURN_ERROR, RequestEvent);

NetworkAcces::NetworkAcces()
{
    SetProxy("", NetworkAcces::NONE);
}

NetworkAcces::~NetworkAcces()
{
}
void NetworkAcces::SetTimeout(long timeout)
{
    timeout_ = timeout;
}
size_t NetworkAcces::WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

void NetworkAcces::SetProxy(std::string addres, NetworkAcces::TypeProxy type, std::string proxyUser, std::string proxyPass)
{
    type_ = type;
    proxyAddres = addres;
    this->proxyUser = proxyUser;
    this->proxyPass = proxyPass;
}

void NetworkAcces::GetRequest(std::string url, std::string idRequest, std::vector<std::string> header)
{
    this->idRequest = idRequest;
    auto as = std::async(std::launch::async, [url, idRequest, header, this] {
        CURL* curl = curl_easy_init();
        std::map<std::string, std::string> postfeilds;

        if (curl)
            startReqest(curl, url, idRequest, postfeilds, NetworkAcces::TypeRequest::GET_R, header);
    });
    as.wait();
}

void NetworkAcces::GetRequest(std::string url, std::string idRequest)
{
    std::vector<std::string> header;
    GetRequest(url, idRequest, header);
}

void NetworkAcces::PostRequest(std::string url, std::string idRequest, std::vector<NetPostData> postData, std::vector<std::string> header)
{
    this->idRequest = idRequest;
    auto as = std::async(std::launch::async, [url, idRequest, postData, header, this] {
        CURL* curl = curl_easy_init();

        if (curl)
            startReqest(curl, url, idRequest, getPostField(postData), NetworkAcces::TypeRequest::POST_R, header);
    });
    as.wait();
}

void NetworkAcces::PostRequest(std::string url, std::string idRequest, std::vector<NetPostData> postData)
{
    std::vector<std::string> header;
    PostRequest(url, idRequest, postData, header);
}

void NetworkAcces::PutRequest(std::string url, std::string idRequest, std::vector<NetPostData> putData, std::vector<std::string> header)
{
    this->idRequest = idRequest;
    auto as = std::async(std::launch::async, [url, idRequest, putData, header, this] {
        CURL* curl = curl_easy_init();

        if (curl)
            startReqest(curl, url, idRequest, getPostField(putData), NetworkAcces::TypeRequest::PUT_R, header);
    });
    as.wait();
}

void NetworkAcces::PutRequest(std::string url, std::string idRequest, std::vector<NetPostData> putData)
{
    std::vector<std::string> header;
    PutRequest(url, idRequest, putData, header);
}

void NetworkAcces::DeleteRequest(std::string url, std::string idRequest, std::vector<NetPostData> delData, std::vector<std::string> header)
{
    this->idRequest = idRequest;
    auto as = std::async(std::launch::async, [url, idRequest, delData, header, this] {
        CURL* curl = curl_easy_init();

        if (curl)
            startReqest(curl, url, idRequest, getPostField(delData), NetworkAcces::TypeRequest::DELETE_R, header);
    });
    as.wait();
}

void NetworkAcces::DeleteRequest(std::string url, std::string idRequest, std::vector<NetPostData> delData)
{
    std::vector<std::string> header;
    DeleteRequest(url, idRequest, delData, header);
}

std::map<std::string, std::string> NetworkAcces::getPostField(std::vector<NetPostData> data)
{
    std::map<std::string, std::string> postfeilds;
    if (!data.empty())
    {
        for (int i = 0; i < data.size(); i++)
        {
            std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
            std::string text_utf8 = myconv.to_bytes(data[i].dataField.ToStdWstring());
            // std::cout << data[i].nameField.ToStdString() << "; " << text_utf8 << std::endl;
            postfeilds[data[i].nameField.ToStdString()] = text_utf8;
        }
    }

    return postfeilds;
}

void NetworkAcces::startReqest(CURL* curl, const std::string& url, const std::string& idRequest, const std::map<std::string, std::string>& postfields, const NetworkAcces::TypeRequest type, const std::vector<std::string>& header)
{
    std::string str_response;
    CURLcode res;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);  // Включаем HTTPS
    curl_mime* mime;
    curl_mimepart* part;
    mime = curl_mime_init(curl);

    // POST Поля
    if (postfields.size() > 0 || type == TypeRequest::POST_R)
    {
        for (const auto& itemPost : postfields)
        {
            part = curl_mime_addpart(mime);
            curl_mime_name(part, itemPost.first.c_str());
            curl_mime_data(part, itemPost.second.c_str(), CURL_ZERO_TERMINATED);
        }
        curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime);
    }

    if (type_ == NetworkAcces::HTTP)
    {
        curl_easy_setopt(curl, CURLOPT_PROXY, proxyAddres.c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    }
    else if (type_ == NetworkAcces::HTTPS)
    {
        curl_easy_setopt(curl, CURLOPT_PROXY, proxyAddres.c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTPS);
    }
    else if (type_ == NetworkAcces::SOCKS4)
    {
        curl_easy_setopt(curl, CURLOPT_PROXY, proxyAddres.c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS4);
    }
    else if (type_ == NetworkAcces::SOCKS5)
    {
        curl_easy_setopt(curl, CURLOPT_PROXY, proxyAddres.c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    }

    if (type_ != NetworkAcces::NONE && proxyUser.size() > 2 && proxyPass.size() > 0)
    {
        std::string userpwd = proxyUser + ":" + proxyPass;
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_ANY);
        curl_easy_setopt(curl, CURLOPT_USERPWD, userpwd.c_str());
    }

    if (type == TypeRequest::PUT_R)
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
    else if (type == TypeRequest::DELETE_R)
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");

    /* отправьте все данные в эту функцию  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

    /* завершено в течение 20 секунд */
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout_);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str_response);

    /* example.com is redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    struct curl_slist* headers = NULL;
    if (!header.empty())
    {
        for (std::string itemHead : header)
        {
            // std::cout << itemHead << std::endl;
            headers = curl_slist_append(headers, itemHead.c_str());
        }

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    }

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if (res != CURLE_OK)
    {
        retError(curl_easy_strerror(res), idRequest);
    }
    else
    {
        retData(str_response, idRequest);
    }
    curl_mime_free(mime);

    /* always cleanup */
    curl_easy_cleanup(curl);
}

void NetworkAcces::retData(std::string data, std::string idRequest)
{
    RequestEvent event(REQ_RETURN_DATA, idRequest, data);
    event.SetEventObject(this);
    // ProcessEvent(event);
    wxPostEvent(this, event);
}

void NetworkAcces::retError(std::string error, std::string idRequest)
{
    RequestEvent event(REQ_RETURN_ERROR, idRequest, error);
    event.SetEventObject(this);
    // ProcessEvent(event);
    wxPostEvent(this, event);
}
