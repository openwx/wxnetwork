#ifndef NETWORKACCES_H
#define NETWORKACCES_H

#define WIN32_LEAN_AND_MEAN
#include <curl/curl.h>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/webrequest.h>
#include <string>
#include <codecvt>
#include <locale>
#include "requestevent.h"
#include <vector>
#include <algorithm>
#include <chrono>
#include <thread>
#include <future>
#include <queue>
#include <map>

class NetPostData
{
public:
    wxString nameField;
    wxString dataField;
};

wxDECLARE_EVENT(REQ_RETURN_DATA, RequestEvent);
wxDECLARE_EVENT(REQ_RETURN_ERROR, RequestEvent);

class NetworkAcces : public wxEvtHandler
{
public:
    enum TypeProxy
    {
        NONE,
        HTTP,
        HTTPS,
        SOCKS4,
        SOCKS5
    };
    NetworkAcces();
    ~NetworkAcces();

    void SetTimeout(long timeout);  // Установка таймоута соединения

    static size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp);

    void SetProxy(std::string addres, NetworkAcces::TypeProxy type, std::string proxyUser = "", std::string proxyPass = "");

    void GetRequest(std::string url, std::string idRequest, std::vector<std::string> header);  // Отправка GET запроса
    void GetRequest(std::string url, std::string idRequest);                                   // Отправка GET запроса

    void PostRequest(std::string url, std::string idRequest, std::vector<NetPostData> postData, std::vector<std::string> header);  // Отправка POST запроса
    void PostRequest(std::string url, std::string idRequest, std::vector<NetPostData> postData);                                   // Отправка POST запроса

    void PutRequest(std::string url, std::string idRequest, std::vector<NetPostData> putData, std::vector<std::string> header);  // Отправка PUT запроса
    void PutRequest(std::string url, std::string idRequest, std::vector<NetPostData> putData);                                   // Отправка PUT запроса

    void DeleteRequest(std::string url, std::string idRequest, std::vector<NetPostData> delData, std::vector<std::string> header);  // Отправка DELETE запроса
    void DeleteRequest(std::string url, std::string idRequest, std::vector<NetPostData> delData);                                   // Отправка DELETE запроса

    std::string idRequest = "Request";

private:
    enum TypeRequest
    {
        GET_R,
        POST_R,
        PUT_R,
        DELETE_R
    };

    NetworkAcces::TypeProxy type_;
    std::string proxyAddres;
    std::string proxyUser;
    std::string proxyPass;

    long timeout_ = 5L;

    std::map<std::string, std::string> getPostField(std::vector<NetPostData> data);

    void startReqest(CURL* curl, const std::string& url, const std::string& idRequest, const std::map<std::string, std::string>& postfields, const NetworkAcces::TypeRequest type, const std::vector<std::string>& header);
    void retData(std::string data, std::string idRequest);
    void retError(std::string error, std::string idRequest);

};

#endif