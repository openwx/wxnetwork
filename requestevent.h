#ifndef REQUESTEVENT_H
#define REQUESTEVENT_H

#include <wx/event.h>
#include <string>

class RequestEvent : public wxEvent
{
public:
    RequestEvent(wxEventType eventType, std::string idRequest, std::string data) : wxEvent(wxID_ANY, eventType), idRequest_(idRequest), data_(data)
    {
    }
    std::string getIdRequest()
    {
        return idRequest_;
    };
    std::string getData()
    {
        return data_;
    };

    // implement the base class pure virtual
    virtual wxEvent* Clone() const
    {
        return new RequestEvent(*this);
    }

private:
    std::string idRequest_;
    std::string data_;
};

#endif
